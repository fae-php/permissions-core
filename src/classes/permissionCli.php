<?php

/**
 * Schema cli manager for FAE
 * Copyright 2019 Callum Smith
 */

namespace FAE\permissions;

use FAE\fae\fae;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Question\ChoiceQuestion;

class permissionCli extends Command
{

  protected static $defaultName = 'app:permissions';

  protected function configure()
  {
    $this
      ->setDescription('Manage permissions and permission groups')
      ->setHelp('Help?')
      ->addOption('load-default-groups', '', InputOption::VALUE_NONE, 'Load default options into the database')
      ->addOption('set-default', '', InputOption::VALUE_NONE, 'Set which permission group you want to be default')
      ->addOption('add-permissions', 'a', InputOption::VALUE_NONE, 'Add permissions defined within frames')
      ->addOption('group-data-json', '', InputOption::VALUE_OPTIONAL, 'Path to the group data json file', __DIR__ . '/../data/perm_group.json');
  }

  protected function execute(InputInterface $input, OutputInterface $output)
  {
    
    if ($input->getOption('load-default-groups')) {
      // Load the default groups into the schema
      $this->loadDefaults($input, $output);
    }
    if ($input->getOption('add-permissions')) {
      // Load the default groups into the schema
      $this->addPermissions($output);
    }
    if ($input->getOption('set-default')) {
      // Load the default groups into the schema
      $this->setDefault($input, $output);
    }
  }

  private function loadDefaults(InputInterface $input, OutputInterface $output)
  {
    if ($output->isVeryVerbose()) {
      $output->writeln('Loading data from file [module]/data/perm_group.json');
    }
    $data = $this->loadData($input->getOption('group-data-json'));
    $ph = new perm_group();
    if ($output->isVeryVerbose()) {
      $groups = count($data);
      $output->writeln("Loaded {$groups} groups");
    }
    $created = 0;
    foreach ($data as $row) {
      if (!$ph->get(['name' => $row->name])->rowCount()) {
        try {
          $ph->insert((array) $row);
        } catch (\Exception $e) {
          $output->writeln("<error>{$e->getMessage()}</error>");
          return;
        }
        if ($output->isVeryVerbose()) {
          $output->writeln("Created group {$row->name}");
        }
        $created++;
      } else {
        if ($output->isVeryVerbose()) {
          $output->writeln("Ignoring group {$row->name} as it already exists in the table");
        }
      }
    }
    if ($output->isVerbose()) {
      $output->writeln("Created {$created} permission groups");
    }
  }

  private function loadData(string $file = __DIR__ . '/../data/perm_group.json')
  {
    $data = json_decode(file_get_contents($file));
    return $data;
  }

  private function setDefault(InputInterface $input, OutputInterface $output)
  {
    $ph = new perm_group();
    $groups = $ph->get();
    $groupOptions = [];
    while ($row = $groups->fetch()) {
      $groupOptions[$row['id']] = $row['name'] . ($row['default'] ? " (current)" : "");
    }
    $helper = $this->getHelper('question');
    $question = new ChoiceQuestion(
      'Please select the group which you would like to be default',
      $groupOptions
    );
    $question->setErrorMessage('Group %s is invalid.');
    $group = $helper->ask($input, $output, $question);
    $perm_group_id = array_search($group, $groupOptions);
    $ph->update(['default' => 0], ['id' => "!{$perm_group_id}"]);
    $ph->update(['default' => 1], ['id' => "{$perm_group_id}"]);
    if ($output->isVerbose()) {
      $output->writeln("{$group} assigned as the default group successfully");
    }
  }

  private function addPermissions(OutputInterface $output)
  {
    $ph = new perm();
    $pgh = new perm_group();
    $pah = new perm_assign();
    $perms = fae::loadOptions('permissions')['perm'];
    if (!empty($perms) && !is_array($perms)) {
      throw new \RuntimeException('Permissions passed via frame.json must be an array of objects');
    }
    if ($output->isVeryVerbose()) {
      $totalPerms = count($perms);
      $output->writeln("Found a total of {$totalPerms} permissions to load");
    }
    $created = 0;
    $updated = 0;
    $perm_array = [];
    foreach ((array) $perms as $perm) {
      $perm_array[$perm->key] = (array) $perm;
      if (!$ph->get(['key' => $perm->key])->rowCount()) {
        if (strlen($perm->key) > 30) {
          $output->writeln("Ignoring {$perm->name} as the key is longer than 30 characters");
          continue;
        }
        try {
          $ph->insert(['key' => $perm->key, 'name' => $perm->name, 'description' => $perm->description, 'options' => ($perm->options ? json_encode($perm->options) : null), 'permanent' => true]);
        } catch (\Exception $e) {
          $output->writeln("<error>{$e->getMessage()}</error>");
          return;
        }
        if ($output->isVeryVerbose()) {
          $output->writeln("Created a permission {$perm->name}");
        }
        $created++;
      } else {
        $ph->update(['name' => $perm->name, 'description' => $perm->description, 'options' => ($perm->options ? json_encode($perm->options) : null), 'permanent' => true], ['key' => $perm->key]);
        if ($output->isVeryVerbose()) {
          $totalPerms = count($perms);
          $output->writeln("Updated '{$perm->name}' from the frame configurations");
        }
        $updated++;
      }
    }
    if ($output->isVerbose()) {
      $output->writeln("Created {$created} and updated {$updated} permissions");
    }
    $groups = $pgh->get();
    $dbPermsResult = $ph->get();
    $dbPerms = $dbPermsResult->fetchAll();
    $permAssigned = 0;
    while ($group = $groups->fetch()) {
      foreach ($dbPerms as $perm) {
        if (!$pah->get(['perm_group_id' => $group['id'], 'perm_id' => $perm['id']])->rowCount()) {
          try {
            $pah->insert([
              'perm_group_id' => $group['id'],
              'perm_id' => $perm['id'],
              'level' => $perm_array[$perm['key']]['default'] ?? $group['level'],
            ]);
          } catch (\Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
            return;
          }
          $permAssigned++;
        }
      }
    }
    if ($output->isVerbose()) {
      $output->writeln("Assigned a total of {$permAssigned} default levels");
    }
  }
}
