<?php

namespace FAE\permissions;

use Doctrine\DBAL\Query\QueryBuilder;
use FAE\auth\auth;
use FAE\schema\model\model;

abstract class permAbstract extends model
{

  // @var bool Whether to skip permissions checks on the model or not
  protected $_direct = false;
  
  function can($id = null, int $level = 0): bool
  {
    if (empty($id)) {
      return false;
    }
    $qb = $this->_conn->createQueryBuilder()
      ->select("COUNT(*) AS `count`")
      ->from('`' . $this->_table . '`', 't1');
    $qb->andWhere("t1.`id` = " . $qb->createNamedParameter($id));
    $qb = perm::addPermissions($level, $qb, $this);
    try {
      $result = $qb->execute();
    } catch (\Exception $e) {
      throw new \RuntimeException($e->getMessage());
    }
    return $result->fetch()['count'] ? true : false;
  }

  public function isDirect(): bool
  {
    return $this->_direct;
  }

  public function setDirect(bool $flag): void
  {
    $this->_direct = $flag;
  }

  function canRead($id = null, array $data = [], int $level = perm::READ): bool
  {
    return $this->can($id, $level);
  }

  function canWrite($id = null, array $data = [], int $level = perm::WRITE, array $filter = []): bool
  {
    if ($id) {
      return $this->can($id, $level);
    } else {
      if ($this->_permRef) {
        $ah = new auth();
        $auth = $ah->get();
        return $auth->verify($this->_permRef, $level);
      }
    }
  }

  function __toString()
  {
    return get_called_class();
  }

  abstract function permissionQBInstance(QueryBuilder $qb, int $level, $userId = null): QueryBuilder;
}
