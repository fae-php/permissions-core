<?php
namespace FAE\permissions;

use FAE\schema\model\model;

class user_perm extends model {
  
  var $_model     = 'user_perm';
  var $_modelFile = __DIR__ . '/../models/user_perm.json';

  var $_rest      = false;
  
}