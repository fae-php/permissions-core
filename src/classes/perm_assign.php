<?php
/**
 * FAE 
 */
namespace FAE\permissions;

use Doctrine\DBAL\Query\QueryBuilder;

use FAE\fae\fae;
use FAE\schema\model\model;

class perm_assign extends model {
  
  var $_model     = 'perm_assign';
  var $_modelFile = __DIR__ . '/../models/perm_assign.json';

  var $_rest      = false;
  
  function getQueryHook( QueryBuilder $qb, array $filter )
  {
    $qb->leftJoin( 't1', 'perm', 't2', 't1.`perm_id` = t2.`id`' );
    if($filter['key']){
      $qb->andWhere('t2.`key` = '.$qb->createNamedParameter($filter['key']));
    }
    return $qb;
  }
  
  static function pageLoader( array $variables )
  {
    // Load data
    if($variables['_loadData']){
      $perm = new perm();
      $assign = new self();
      $group = new perm_group();
      $variables['perm'] = $perm->get()->fetchAll();
      foreach($variables['perm'] as $key => $row){
        $assignData = $assign->get(['perm_group_id' => $variables['id'], 'perm_id' => $row['id']])->fetch();
        $variables['perm'][$key]['level'] = $assignData['level'];
      }
      $variables['group'] = $group->get(['id' => $variables['id']])->fetch();
    }
    if(class_exists('\\FAE\\template\\layout')){
      $layout = new \FAE\template\layout( (object) $variables );
      $layout->renderLayout();
    }
  }
  
  static function setApi( array $variables )
  {
    $permGroupId = $variables['id'];
    $permAssign = new self();
    try {
      $permAssign->_conn->beginTransaction();
      foreach($_POST['perm'] as $permId => $level){
        if($permAssign->get(['perm_id' => $permId, 'perm_group_id' => $permGroupId])->rowCount()){
          $permAssign->update(['level' => $level], ['perm_id' => $permId, 'perm_group_id' => $permGroupId]);
        } else {
          $permAssign->insert(['level' => $level, 'perm_id' => $permId, 'perm_group_id' => $permGroupId]);
        }
      }
      $permAssign->_conn->commit();
      \FAE\rest\rest::output(['success' => true]);
    } catch (\Exception $e){
      \FAE\rest\rest::errorDisplay($e);
    }
  }
  
}