<?php
/**
 * FAE 
 */
namespace FAE\permissions;

use FAE\schema\model\model;

class user_perm_group extends model {
  
  var $_model     = 'user_perm_group';
  var $_modelFile = __DIR__ . '/../models/user_perm_group.json';
  
}