<?php
/**
 * FAE 
 */
namespace FAE\permissions;

use FAE\schema\model\model;

class perm_group extends model {
  
  var $_model     = 'perm_group';
  var $_modelFile = __DIR__ . '/../models/perm_group.json';

  var $_rest      = false;
  
}