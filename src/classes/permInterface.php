<?php
/**
 * FAE 
 *
 * Copyright 2019 Callum Smith
 */
namespace FAE\permissions;

use FAE\schema\model\model;
use FAE\auth_local\auth;

use Doctrine\DBAL\Query\QueryBuilder;

class permInterface extends model {
  
  const DENY      = 0;
  const READ      = 1;
  const DRAFT     = 3;
  const WRITE     = 6;
  const APPROVE   = 8;
  const ADMIN     = 11;
  
  function can( int $id, int $level ) :bool {
    if(!is_int($id)){
      return false;
    }
    $qb = $this->_conn->createQueryBuilder()
                      ->select("COUNT(*) AS `count`")
                      ->from( '`'.$this->_table.'`', 't1' );
    $qb->andWhere("t1.`id` = ".$qb->createNamedParameter($id));
    $qb = perm::addPermissions($level, $qb, $this);
    try {
      $result = $qb->execute();
    } catch( \Exception $e ){
      throw new \RuntimeException($e->getMessage());
    }
    return $result->fetch()['count'] ? true : false;
  }
  
  function canRead(int $id, int $level = self::READ) :bool {
    return $this->can($id, $level);
  }
  
  function canWrite(int $id, int $level = self::WRITE) :bool {
    return $this->can($id, $level);
  }
}