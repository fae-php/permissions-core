<?php

/**
 * FAE 
 *
 * Copyright 2019 Callum Smith
 */

namespace FAE\permissions;

use FAE\fae\fae;
use FAE\schema\model\model;
use FAE\auth\auth;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Query\Expression\CompositeExpression;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use FAE\auth\authException;
use RuntimeException;

class perm extends model
{

  const DENY      = 0;
  const READ      = 1;
  const DRAFT     = 3;
  const WRITE     = 6;
  const APPROVE   = 8;
  const ADMIN     = 11;

  var $_model     = 'perm';
  var $_modelFile = __DIR__ . '/../models/perm.json';

  var $_rest      = false;

  static $permCache = [];

  /**
   * @param QueryBuilder $qb The parent query builder interface
   * @param model $instance The instance on which the permissions are being extended to
   *
   * @return QueryBuilder|string Configured where stack of arguments to be appended to a query
   */
  function buildStandardPermissions(QueryBuilder $qb, int $level, permAbstract $instance, $userId = null)
  {
    if ($instance->isDirect()) {
      return "1 = 1";
    }
    $ah = new auth();
    $auth = $ah->get();
    $perm_group_id = $auth->getPermGroup();
    $perm_ref = $instance->_permRef;
    // If the permission level has already been calculated load from static cache
    if (array_key_exists($perm_group_id, self::$permCache) && array_key_exists($perm_ref, self::$permCache[$perm_group_id]) &&  array_key_exists($level, self::$permCache[$perm_group_id][$perm_ref])) {
      $allow = self::$permCache[$perm_group_id][$perm_ref][$level];
      // Otherwise query the permissions database
    } else {
      $filterQb = $this->_conn->createQueryBuilder();
      $filterQb->select('pa.`id`')
        ->from('`perm_assign`', 'pa')
        ->join('pa', '`perm`', 'p', 'pa.`perm_id` = p.`id`')
        ->andWhere($filterQb->expr()->eq('p.`key`', $filterQb->createNamedParameter($perm_ref)))
        ->andWhere($filterQb->expr()->eq('pa.`perm_group_id`', $perm_group_id))
        ->andWhere($filterQb->expr()->gte('pa.`level`', $level));
      $result = $filterQb->execute();
      $allow = count($result->fetchAll()) > 0;
      self::$permCache[$perm_group_id][$perm_ref][$level] = $allow;
    }
    // Return a binary MySQL evaluation to exclude or include results
    if ($allow) {
      return "1 = 1";
    } else {
      return "1 = 0";
    }
  }

  static function verify($perm_id, int $level)
  {
    $ah = new auth();
    $auth = $ah->get();
    return $auth->verify($perm_id, $level);
  }

  static function getProtectedCols(model $instance, array $columns)
  {
    if (!isset($instance->_protectedCols) || !is_array($instance->_protectedCols)) {
      return $columns;
    }
    foreach ($instance->_protectedCols as $col => $level) {
      if (in_array($col, $columns) && !self::verify($instance->_permRef, $level)) {
        unset($columns[array_search($col, $columns)]);
      }
    }
    return $columns;
  }

  /**
   * Undocumented function
   *
   * @param permAbstract $instance
   * @param array $data
   * @return array
   */
  static function deletePermissionsTest($instance, array $filter): array
  {
    return self::setPermissionsTest($instance, $filter, $filter);
  }

  /**
   * Undocumented function
   *
   * @param permAbstract $instance
   * @param array $data
   * @return array
   */
  static function setPermissionsTest($instance, array $data, array $filter): array
  {
    if (!$instance instanceof permAbstract) {
      return $data;
    }
    if($instance->isDirect()){
      return $data;
    }
    if (method_exists($instance, "canWrite")) {
      if ( !$instance->canWrite( isset($filter['id']) ? $filter['id'] : null , $data, self::WRITE, $filter)) {
        throw new permException("Permission denied writing object {$instance->__toString()}", 403);
      }
      return $data;
    }
    $ah = new auth();
    $auth = $ah->get();
    if ($instance->_passRevision === false) {
      if (!$auth->verify($instance->_permRef, self::DRAFT)) {
        throw new permException('Permission denied when writing draft', 403);
      }
    } else {
      if (!$auth->verify($instance->_permRef, self::WRITE)) {
        throw new permException('Permission denied writing object', 403);
      }
    }
    return $data;
  }

  /**
   *
   * @param $level
   */
  static function getPermissionsQb(QueryBuilder $qb, array $filter = [], array $columns = [], model $instance = null): QueryBuilder
  {
    if (!$instance instanceof permAbstract) {
      return $qb;
    }
    if($instance->isDirect()){
      return $qb;
    }
    $level = self::READ;
    // Add Hook in here
    self::addPermissions($level, $qb, $instance);
    return $qb;
  }

  /**
   *
   * @param $level
   */
  static function addPermissions(int $level, QueryBuilder $qb, permAbstract $instance): QueryBuilder
  {
    try {
      if (method_exists($instance, 'permissionQBInstance')) {
        $ah = new auth();
        $auth = $ah->get();
        $instance->permissionQBInstance($qb, $level, $auth->getUserID());
      }
    } catch (authException $e) {
      throw $e;
    } catch (\RuntimeException $e) {
      // Add monolog
      throw $e;
    }
    return $qb;
  }
}
