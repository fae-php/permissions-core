<?php
namespace FAE\permissions;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use FAE\fae\fae;
use FAE\fae\routes;

$route = new RouteCollection();
$route->add( 'perm-group-perm', new Route(
    '/adm/perm-group/perm/{id}',
    [
      '_dataController' => '\\FAE\\permissions\\perm_assign',
      '_method'       => 'perm-assign',
      '_action'       => 'read',
      '_controller'   => '\\FAE\\permissions\\perm_assign::pageLoader',
      '_page'         => 'perm.assign.html.twig',
      '_loadData'     => true,
    ],
    ['id' => '[0-9]+'],
    [],
    '',
    [],
    ['GET']
  )
);
$route->add( 'perm-group-perm-api', new Route(
    "/api/{$config->apiVersion}/perm-group/perm/{id}",
    [
      '_controller'   => '\\FAE\\permissions\\perm_assign::setApi',
    ],
    ['id' => '[0-9]+'],
    [],
    '',
    [],
    ['POST']
  )
);
$route->setSchemes(['https','http']);

routes::addCollection($route);